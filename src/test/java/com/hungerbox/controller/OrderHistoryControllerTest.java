package com.hungerbox.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hungerbox.dto.OrderItemsListResponseDto;
import com.hungerbox.model.Order;
import com.hungerbox.model.OrderItemList;
import com.hungerbox.service.OrderHistoryService;



@RunWith(MockitoJUnitRunner.Silent.class)
public class OrderHistoryControllerTest {
	
	
	@InjectMocks
	OrderHistoryController orderHistoryController;
	
	@Mock
	OrderHistoryService orderHistoryService; 
	
	@Test
	public void TestGetByEmployeeIdForPositive() {
		
		List<Order> list=new ArrayList<>();
		OrderItemList orderItems=new OrderItemList();
		orderItems.setId(1L);
		
		
		Mockito.when(orderHistoryService.lastFiveTransactionsHistory(Mockito.anyLong())).thenReturn(list);
		ResponseEntity<List<OrderItemsListResponseDto>> result=orderHistoryController.showLastFiveTransactions(Mockito.anyLong());
		assertEquals( HttpStatus.OK,result.getStatusCode());
		
	}

	
	@Test
	public void TestGetByEmployeeIdForNegative() {
		
		List<Order> list=new ArrayList<>();
		OrderItemList orderItems=new OrderItemList();
		orderItems.setId(-1L);
		
		Mockito.when(orderHistoryService.lastFiveTransactionsHistory(Mockito.anyLong())).thenReturn(list);
		ResponseEntity<List<OrderItemsListResponseDto>> result=orderHistoryController.showLastFiveTransactions(Mockito.anyLong());
				
		assertEquals( HttpStatus.OK,result.getStatusCode());
		
	}

	
}