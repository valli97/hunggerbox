package com.hungerbox.service;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import com.hungerbox.model.Employee;
import com.hungerbox.model.Item;
import com.hungerbox.model.Order;
import com.hungerbox.model.OrderItemList;
import com.hungerbox.model.Vendor;
import com.hungerbox.repository.EmployeeRepository;
import com.hungerbox.repository.OrderItemListRepository;



@RunWith(MockitoJUnitRunner.Silent.class)
public class OrderHistoryServiceTest {
	
	@InjectMocks
	OrderHistoryService historyServiceImpl;
	
	@Mock
	EmployeeRepository employeeRepository;

	@Mock
	OrderItemListRepository orderItemRepository;

	@Test
	public void TestOrderListByEmployeeForPositive() {
		
		Vendor vendor=new Vendor();
		vendor.setStallNumber(7670);
		vendor.setVendorId(1L);
		vendor.setVendorName("dosatalks");
		
		Item item=new Item();
		item.setItemDescription("suprrr");
		item.setItemId(1L);
		item.setItemType("veg");
		item.setName("doasa");
		item.setUnitPrice(30.0);
		item.setVendor(vendor);
	
		Employee emp=new Employee();
		emp.setEmail("dhaya@gmail.com");
		emp.setEmployeeId(1L);
		emp.setEmployeeName("dhayananthan");
		emp.setPassword("778249day");
		emp.setPhone("887044827");
		
		Order order=new Order();
		order.setEmployee(emp);
		order.setOrderId(1L);
		order.setOrderPrice(80.0);
		
			
		List<OrderItemList> list=new ArrayList<>();
		OrderItemList orderItems=new OrderItemList();
		orderItems.setId(1L);
		orderItems.setItem(item);
		orderItems.setOrder(order);
		list.add(orderItems);
		
		Mockito.when(employeeRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(emp));
		Mockito.when(orderItemRepository.findOrderItemListByOrder(Mockito.any())).thenReturn(list);
				
		
		List<Order> result=historyServiceImpl.lastFiveTransactionsHistory(emp.getEmployeeId());
		assertNotNull(result);
		
		
	}
	
	@Test
	public void TestOrderListByEmployeeForNegative() {
		
		Vendor vendor=new Vendor();
		vendor.setStallNumber(7670);
		vendor.setVendorId(-1L);
		vendor.setVendorName("dosatalks");
		
		Item item=new Item();
		item.setItemDescription("suprrr");
		item.setItemId(-1L);
		item.setItemType("veg"); 
		item.setName("doasa");
		item.setUnitPrice(30.0);
		item.setVendor(vendor);
	
		Employee emp=new Employee();
		emp.setEmail("dhaya@gmail.com");
		emp.setEmployeeId(1L);
		emp.setEmployeeName("dhayananthan");
		emp.setPassword("778249day");
		emp.setPhone("987898788");
		
		Order order=new Order();
		order.setEmployee(emp);
		order.setOrderId(1L);
		order.setOrderPrice(80.0);
		
			
		List<OrderItemList> list=new ArrayList<>();
		OrderItemList orderItems=new OrderItemList();
		orderItems.setId(1L);
		orderItems.setItem(item);
		orderItems.setOrder(order);
		list.add(orderItems);
		
		Mockito.when(employeeRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(emp));
		Mockito.when(orderItemRepository.findById((Mockito.any())).thenReturn(list);
		
		List<Order> result=historyServiceImpl.lastFiveTransactionsHistory(emp.getEmployeeId());
				assertNotNull(result);
		
	}
	
}
