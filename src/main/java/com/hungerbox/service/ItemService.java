package com.hungerbox.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hungerbox.exceptions.ItemNotFoundException;
import com.hungerbox.model.Item;
import com.hungerbox.repository.ItemRepository;

/**
 * 
 * @author mounika
 * this is class for item service
 *
 */

@Service
public class ItemService {
	
	Logger logger = LoggerFactory.getLogger(ItemService.class);
	
	@Autowired
	ItemRepository itemRepository;
	
	public List<Item> AccessItemByName(String name) throws ItemNotFoundException {
		List<Item> items = itemRepository.findItemByNameLike("%" + name + "%");
			 if(items.isEmpty()) {
				 throw new ItemNotFoundException("item not found with the given name");
			 } 
			 return items;
		}



}
