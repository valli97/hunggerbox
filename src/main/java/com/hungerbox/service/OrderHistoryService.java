package com.hungerbox.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hungerbox.dto.OrderItemsListResponseDto;
import com.hungerbox.exceptions.EmployeeNotFoundException;
import com.hungerbox.model.Employee;
import com.hungerbox.model.Order;
import com.hungerbox.model.OrderItemList;
import com.hungerbox.repository.EmployeeRepository;
import com.hungerbox.repository.OrderItemListRepository;
import com.hungerbox.repository.OrderRepository;

/**
 * 
 * @author valli
 *
 */
@Service
public class OrderHistoryService {
	Logger logger = LoggerFactory.getLogger(OrderHistoryService.class);
	
	@Autowired
	OrderItemListRepository orderItemListRepository;
	
	@Autowired
	OrderRepository orderRepository;
	
	@Autowired
	EmployeeRepository employeeRepository;
	
	public List<OrderItemsListResponseDto> showLastTransactions(long employeeId){
		List<Order> order=lastFiveTransactionsHistory(employeeId);
		List<OrderItemsListResponseDto> resultOrderList = new ArrayList<>();
		for(int i=0;i<order.size();i++) {
			
			OrderItemsListResponseDto orderDto = new OrderItemsListResponseDto();
			List<OrderItemList> orderList = orderItemListRepository.findOrderItemListByOrder(order.get(i));
			orderDto.setOrderId(order.get(i).getOrderId());
			 orderDto.setItems(orderList); 
			 resultOrderList.add(orderDto);
		} 
		return resultOrderList;
	}
	
	public List<Order> lastFiveTransactionsHistory(long employeeId){
		Employee employee = employeeRepository.findById(employeeId).orElseThrow(()->new EmployeeNotFoundException("employee with given id not found"));
		List<Order> order = orderRepository.findOrderByEmployee(employee);
		Collections.reverse(order);
		List<Order> order1 = order.stream().limit(5).collect(Collectors.toList());
		return order1;
	}
}
