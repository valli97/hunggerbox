package com.hungerbox.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hungerbox.model.Order;
import com.hungerbox.model.OrderItemList;

public interface OrderItemListRepository extends JpaRepository<OrderItemList, Long> {

	List<OrderItemList> findOrderItemListByOrder(Order order);

}
