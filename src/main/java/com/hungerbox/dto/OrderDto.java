package com.hungerbox.dto;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class OrderDto {
	@NotEmpty
	private String phoneNo;
	@NotNull
	private  Long employeeId;
	private List<ItemDto> itemDto;
	
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public Long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}
	public List<ItemDto> getItemDto() {
		return itemDto;
	}
	public void setItemDto(List<ItemDto> itemDto) {
		this.itemDto = itemDto;
	}
	
}
